function filter = gaussianLowPass(M, N, filterLength)
    filter = zeros(M,N,'double');
    
    for i=1:M
        for j=1:N
            
            filter(i,j) =  exp(-((sqrt((i - M/2)^2+ (j - N/2)^2)^2) / (2 * filterLength^2 )));       
        
        end
    end
end
