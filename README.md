# Image Processing: Blood vessel Segmentation

I produced this report during my final year at RMIT, I completed my Honors degree in Electrical and Electronics Engineering. 
Please consider reading the full [report](https://gitlab.com/Maddison/blood-vessel-segmentation/-/blob/master/Image%20Processing:%20Segmentation%20of%20Blood%20vessels.pdf) for a more through breakdown and analysis.


### What 
The idea was to take the blood vessel image and apply a variety of techniques that were 
taught in the first half of the semester. The final goal was Segmentation of the blood vessels. 


### How 
Was completely up to me and I could spend as much or as little time as I wanted on it. 


### Results
After doing some research, I came across *"Auto-segmentation of Retinal Blood Vessels Using Image Processing" by Malak T.* 
Malak proposed a series of DSP techniques which were used to segment the retinal blood vessels of the image. The results were impressive.


#### Original Image:
<img src="vessel.jpg" width="100">

#### Final Segmented Image:
<img src="seg_vessel.png" width="100">


The final results of the segmented image can be adjusted through various weightings
which control thresholds, de-noising, and more.

I hope this helps someone one-day!

Enjoy and if you have any questions I am happy to talk.