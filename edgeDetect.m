function edgeImage = edgeDetect(Image, Kernal, testNumber)
    if ~exist('testNumber','var')
        testNumber = 1;
    end
    
    % Size of the image
    [x, y] = size(Image);

    % New image 
    edgeImage = zeros(x,y);

    Gx = Kernal;
    Gy = Gx';

    % Control loop 1
    for i=2:x-1
        % Control loop 2
       for j=2:y-1

            % Creating subimage
            subImage = double(Image(i-1:i+1, j-1:j+1));

            % Getting sum of the product of the subimage with the kernals
            pixelX = sum(sum(Gx .* subImage));
            pixelY = sum(sum(Gy .* subImage));

            % Running different test number
            if testNumber == 1
                % Calculating new pixel val
                val = ceil(sqrt((pixelX * pixelX) + (pixelY * pixelY)));
            elseif testNumber == 2
                val = pixelX;
            else
                val = pixelY;

            end

            % Placing pixel into new image
            edgeImage(i,j) = val;

       end

       edgeImage = uint8(edgeImage);

    end

end
