% Highpass Image Filter:
% Original = Original Image
% windowType = 'gaussian'
% normWn1 = normalized (0-1) cutoff frequency
% 
function filteredImage = highPassImage(Original, windowType, normWn1)
    
    %
    image = Original;
    
    %
    imageSize = size(image);

    %
    [f1,f2] = freqspace(imageSize,'meshgrid');

    %
    r = sqrt(f1.^2 + f2.^2);

    %
    Hd = ones(imageSize); 
    Hd((r<=normWn1)) = 0;

    % window
    win = fspecial(windowType,21,4);
    win = win./max(win(:));
    %
    h = fwind2(Hd,win);
    
    %
    H = freqz2(h, imageSize);
    H = H./max(H(:));

    %
    fftImage = fftshift(fft2(image));
   
    %
    fftFiltered = fftImage.*H;
   
    %
    filteredImage = (real(ifft2(ifftshift(fftFiltered))));

end